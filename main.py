import io
import tkinter as tk
import tkinter.ttk as ttk
from functools import partial


"""
It's simple:
	Start with loading file and store data.
	Call widget to get user plans, process it and join with previous data.
	Show user list of plans to decide what to do with it.
	User can:
		Add new plans
		Edit old plans by double-clicking on it
		Discard the unnecessary ones
		Mark it half-done to save for later
	By the end - write whats left to file.
Have a productive day.


P.S. You may also try fonts:
Arial, Calibri, Cambria, Courier, Georgia,
Helvetica, Lucida, Noto Mono, Noto Sans,
Segoe UI, Sitka Text, Times New Roman, Verdana

"""

VERSION = 1.05
FONT_SIZE = 13
REC_FIELD_WIDTH = 320
TEXT_FONT_FAMILY = 'Segoe UI'
WRITTINGS_FONT_FAMILY = 'Segoe UI'
FILE_WITH_DATA = "./plans_raw.txt"
ENTRY_BG_COLOUR = 'Snow'
SWITCH_OFF_COLOUR = 'FloralWhite'
SWITCH_ON_COLOUR = 'LightSeaGreen'


class PlanBuilder(object):
	"""docstring for PlanBuilder:
	class contains methods for file manipulations and
	an inner class for tkinter application."""
	def __init__(self):
		self.records = self.read_file()
		print("Starting with", len(self.records), '-', '; '.join(self.records))
		root = self.Application(self)
		root.mainloop()
		print("Left", len(self.records), '-', '; '.join(self.records))
		self.write_to_file(self.records)

	def clear_text_str(self, text:str) -> set:
		texttolist = [line.strip() for line in text.split('\n')]
		return set(filter(None, texttolist))

	def read_file(self) -> set:
		reader = io.TextIOBase()
		try:
			reader = io.open(FILE_WITH_DATA, 'r')
		except Exception as e:
			print(e, "\nEmpty list will be used instead.")
			return set()
		else:
			return self.clear_text_str(reader.read())
		finally:
			reader.close()

	def write_to_file(self, records:set) -> None:
		with open(FILE_WITH_DATA, 'w') as writter:
			writter.write('\n'.join(records))

	class Application(tk.Tk):
		"""docstring for Application: provides work of tkinter widgets"""
		def __init__(self, parent):
			super(PlanBuilder.Application, self).__init__()
			self.parent = parent
			self.title(f"{self.parent.__class__.__name__} v{VERSION}")
			self.record_width = REC_FIELD_WIDTH
			self.record_height = int(FONT_SIZE * 2.4)
			self.window_height = 12 * self.record_height
			self.geometry("{}x{}+{}+{}".format(self.record_width,
											   self.window_height,
											   self.winfo_screenwidth()//3,
											   self.winfo_screenheight()//3))
			self.minsize(self.record_width, 2 * self.record_height)
			self.regularfont = (TEXT_FONT_FAMILY, FONT_SIZE)
			self.buttonfont = (WRITTINGS_FONT_FAMILY, FONT_SIZE - 1)
			self.headerfont = (WRITTINGS_FONT_FAMILY, FONT_SIZE + 2)
			self.ttkstyle = ttk.Style()
			self.ttkstyle.configure('TButton', font=self.buttonfont)
			self.sizegrip = ttk.Sizegrip(self)

			startframe = tk.Frame(self)
			startframe.pack(side='top', fill='both', expand=True)
			ttk.Label(startframe, text="PLAN FOR TODAY:", font=self.headerfont).pack()
			startframe.scrollbar = tk.Scrollbar(startframe)
			startframe.scrollbar.pack(side='right', fill='y')
			startframe.text = tk.Text(startframe,
									  height=12,
									  width=self.record_width - int(startframe.scrollbar['width']),
									  font = self.regularfont,
									  bg=ENTRY_BG_COLOUR,
									  yscrollcommand=startframe.scrollbar.set)
			startframe.text.bind('<Key>', self.keyeventhandler)
			startframe.scrollbar.config(command=startframe.text.yview)
			startframe.proceedbutton = ttk.Button(startframe,
												  text='Continue',
												  command=partial(self.switch_to_default_view,
												  				  startframe),
												  )
			startframe.proceedbutton.bind('<Return>',
										  lambda event: startframe.proceedbutton.invoke())
			startframe.proceedbutton.pack(side='bottom')
			startframe.text.pack(side='left', fill='both')
			startframe.text.focus_set()
			self.sizegrip.pack(side="right", anchor='se')
		
		def keyeventhandler(self, event):
			if 3 < event.state < 9: # ctrl+ combination
				if event.char == '\x01': # some cases for 'A'
					event.widget.event_generate('<<SelectAll>>') # from root.bind_class('Text')
				elif event.char == '\x7f': # ctrl+backspace
					event.widget.event_generate('<<SelectPrevWord>>')
				elif event.char == '\n':
					event.widget.master.proceedbutton.event_generate('<<Invoke>>')

		def switch_to_default_view(self, oldframe):
			text_from_frame = oldframe.text.get(1.0, 'end-1c')
			if text_from_frame:
				self.parent.records.update(self.parent.clear_text_str(text_from_frame))
			oldframe.destroy()
			self.sizegrip.pack_forget()

			approx_height = 20 + self.record_height * (1 + len(self.parent.records))
			final_height = min(720, max(approx_height, self.window_height))
			self.geometry(f"{self.record_width}x{final_height}")
			self.text_pack_options = {'side':'left',
									  'fill':'both',
									  'expand':True}
			self.sizegrip.pack(side='bottom', anchor='se')

			"""widget for adding a plan"""
			entryframe = ttk.Frame(self,
								   width=self.record_width,
								   height=self.record_height,
								   borderwidth=1,
								   relief='groove')
			entryframe.entryfield = tk.Entry(entryframe)
			entryframe.sample = "Enter task:"
			entryframe.entryfield.insert(0, entryframe.sample)
			entryframe.entryfield.config(fg='Grey', bg=ENTRY_BG_COLOUR, font=self.regularfont)
			entryframe.proceedbutton = ttk.Button(entryframe, text='Add', width=8)
			entryframe.proceedbutton.pack(side='right')
			for trigger in ['<Button-1>', '<Return>', '<<Invoke>>']:
				entryframe.proceedbutton.bind(trigger, self.entry_get_record)
			entryframe.entryfield.pack(**self.text_pack_options)
			entryframe.entryfield.bind('<Key>', self.keyeventhandler)
			entryframe.entryfield.bind('<FocusIn>', self.entry_handle_focus)
			entryframe.entryfield.bind('<FocusOut>', self.entry_handle_focus)
			entryframe.pack(side='top', fill='x')

			"""widget for scrolling plans"""
			localscrollbar = tk.Scrollbar(self)
			localcanvas = tk.Canvas(self,
									bd=0,
									highlightthickness=0,
									yscrollcommand=localscrollbar.set)
			localcanvas.pack(side='top', fill='both', expand=True)
			localscrollbar.config(command=localcanvas.yview)

			"""widget for displaying plans"""
			self.mainframe = tk.Frame(localcanvas)
			switchsize = FONT_SIZE + 2
			self.mainframe.switches = [tk.PhotoImage(width=switchsize,
									   height=switchsize) for _ in range(2)]
			self.mainframe.switches[0].put(("Grey75",), to=(0, 0, switchsize, switchsize))
			self.mainframe.switches[0].put(SWITCH_OFF_COLOUR, to=(1, 1, switchsize-1, switchsize-1))
			self.mainframe.switches[1].put(SWITCH_ON_COLOUR, to=(0, 0, switchsize-1, switchsize-1))
			for textline in self.parent.records:
				self.record_draw(textline)
			mainframe_id = localcanvas.create_window(0, 0, window=self.mainframe, anchor='nw')

			def _canvas_resize_event(event):
				if self.mainframe.winfo_reqwidth() != localcanvas.winfo_width():
					localcanvas.itemconfigure(mainframe_id, width=localcanvas.winfo_width())

			def _canvas_scrollregion_handle(event):
				localcanvas.config(scrollregion=localcanvas.bbox("all"))

			def _wheel_scroll_handle(event):
				# coefficient is not based on something and you may set your own
				speed = self.winfo_height() // 42 # should be ~8-12 in normal case
				if event.delta > 0: speed = -speed # on Unix might require event.num check
				localcanvas.yview_scroll(speed, 'units')

			localcanvas.bind('<Configure>', _canvas_resize_event)
			self.mainframe.bind('<Configure>', _canvas_scrollregion_handle)
			self.bind('<MouseWheel>', _wheel_scroll_handle)
			
		
		def entry_handle_focus(self, event):
			if repr(event) == '<FocusIn event>':
				event.widget.config(fg='Black')
				if event.widget.get() == event.widget.master.sample:
					event.widget.delete(0, last='end')
			else: # <FocusOut event>
				if not event.widget.get():
					event.widget.config(fg='Grey')
					event.widget.insert(0, event.widget.master.sample)

		def entry_get_record(self, event):
			text = event.widget.master.entryfield.get()
			if text != event.widget.master.sample:
				event.widget.master.entryfield.delete(0, last='end')
				text = text.strip()
				if text:
					if text in self.parent.records:
						print(text, "- already in list.")
					else:
						self.parent.records.add(text)
						self.record_draw(text)

		def record_draw(self, text:str):
			subframe = ttk.Frame(self.mainframe,
								 width=self.record_width,
								 height=self.record_height,
								 borderwidth=1,
								 relief='groove')
			subframe.task = tk.Label(subframe, text=text, font=self.regularfont, anchor='w')
			subframe.task.bind('<Double-Button-1>', self.record_edit)
			subframe.button = ttk.Button(subframe,
										 text='Done',
										 width=8,
										 command=partial(self.record_done, subframe))
			subframe.switch_status : bool = False
			subframe.switch = tk.Button(subframe,
										image=self.mainframe.switches[subframe.switch_status],
										relief='flat',
										command=partial(self.record_half_done, subframe))
			subframe.switch.pack(side='left', fill='y')
			subframe.button.pack(side='right', fill='y')
			subframe.task.pack(**self.text_pack_options)
			subframe.pack(side='top', fill='x')

		def record_half_done(self, frame):
			frame.switch_status = not frame.switch_status
			frame.switch.config(image=self.mainframe.switches[frame.switch_status])
			frame.task.config(font=self.regularfont + ('overstrike',) * frame.switch_status)

		def record_done(self, frame):
			frame.task.config(font=self.regularfont + ('overstrike',))
			frame.task.update()
			print('Done', frame.task['text'])
			frame.after(200)
			self.record_widget_destroy(frame)

		def record_widget_destroy(self, widget):
			self.parent.records.remove(widget.task['text'])
			widget.destroy()

		def record_edit(self, event):
			event.widget.pack_forget()
			masterframe = event.widget.master
			editentry = tk.Entry(masterframe, font=self.regularfont)
			masterframe.proceedbutton = ttk.Button(masterframe, text='Ok', width=4)
			masterframe.proceedbutton.config(command=partial(self.record_edit_done,
											  				 masterframe,
											  				 editentry,
											  				 masterframe.proceedbutton))
			masterframe.proceedbutton.pack(side='right')
			editentry.pack(**self.text_pack_options)
			editentry.bind('<Return>', lambda event: masterframe.proceedbutton.invoke())
			editentry.bind('<Key>', self.keyeventhandler)
			editentry.insert(0, event.widget['text'])
			editentry.focus_set()

		def record_edit_done(self, recordframe, entry, button):
			oldtext = recordframe.task['text']
			newtext = entry.get().strip()
			if newtext:
				if newtext == oldtext:
					pass
				elif newtext in self.parent.records:
					entry.insert('end', " - already exists")
					entry.focus_set()
					return
				else:
					recordframe.task.config(text=newtext)
					self.parent.records.add(newtext)
					self.parent.records.remove(oldtext)
				entry.destroy()
				button.destroy()
				recordframe.task.pack(**self.text_pack_options)
				recordframe.task.bind('<Double-Button-1>', self.record_edit)
			else:
				self.record_widget_destroy(recordframe)


if __name__ == "__main__":
	PlanBuilder()
