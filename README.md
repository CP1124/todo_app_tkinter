# PlanBuilder v1.02

Simple python application built with tkinter 8.6. Has been created as an attempt not to waste paper on such things.<br>
Looks like many other To-Do apps, but has a dialogue window on the start for capturing the nessessary things at the moment and not to be distracted by what's left previously.<br>
Controls are also simple: double-click for edit, check-box on the left for temporary accomplishment, "Done" for delete.<br>
Playing around with fonts and colours is recommended. Luckily they are global parameters.<br>
List of fonts can be found in tk.font.families(). As for colours better check [colours man page](https://www.tcl.tk/man/tcl/TkCmd/colors.html) for Tk.<br>
Dark mode will be added as soon as the most acceptable colour scheme will be found.

----

<img src="https://gitlab.com/CP1124/todo_app_tkinter/-/raw/main/Examples/PlanBuilder-v102-2022-06-17.gif" width="426" height="496" />


## Project status
Almost finished and perhaps wont be supported for new features since they wont make any big change for such kind of a project.<br>
Though a version using PyQt or wxPython may appear.<br>
Thank you for your attention.
